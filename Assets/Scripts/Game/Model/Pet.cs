﻿using System;
using System.ComponentModel;
using UnityEngine;

namespace Assets.Scripts.Game.Controller.Model
{
    public class Pet
    {
        private string name;
        private int life;
        private int attack;
        private int defense;
        private float moveSpeed;

        public Pet(string name, int life, int attack, int defense, float moveSpeed, float attackSpeed, int candyCost, string extraSkill, float attackRange)
        {
            this.name = name;
            this.life = life;
            this.attack = attack;
            this.defense = defense;
            this.moveSpeed = moveSpeed;
            this.attackSpeed = attackSpeed;
            this.candyCost = candyCost;
            this.extraSkill = extraSkill;
            this.attackRange = attackRange;
        }

        public Pet()
        {
        }

        public string Name
        {
            get => name;
            set => name = value;
        }

        public int Life
        {
            get => life;
            set => life = value;
        }

        public int Attack
        {
            get => attack;
            set => attack = value;
        }

        public int Defense
        {
            get => defense;
            set => defense = value;
        }

        public float MoveSpeed
        {
            get => moveSpeed;
            set => moveSpeed = value;
        }

        public float AttackSpeed
        {
            get => attackSpeed;
            set => attackSpeed = value;
        }

        public int CandyCost
        {
            get => candyCost;
            set => candyCost = value;
        }

        public string ExtraSkill
        {
            get => extraSkill;
            set => extraSkill = value;
        }

        public float AttackRange
        {
            get => attackRange;
            set => attackRange = value;
        }

        private float attackSpeed;
        private int candyCost;
        private string extraSkill;
        private float attackRange;

        string getIndexValue(string str,int index)
        {
            return str.Split(',')[index];
        }
        public override string ToString()
        {
            return $"{nameof(name)}: {name}, {nameof(life)}: {life}, {nameof(attack)}: {attack}, {nameof(defense)}: {defense}, {nameof(moveSpeed)}: {moveSpeed}, {nameof(attackSpeed)}: {attackSpeed}, {nameof(candyCost)}: {candyCost}, {nameof(extraSkill)}: {extraSkill}, {nameof(attackRange)}: {attackRange}";
        }

        public Pet(PetData lds)
        {
      
                Name = lds.PetName;
                Life = Int32.Parse(lds.PetLife);
                Attack = Int32.Parse(lds.PetAttack);
                Defense = Int32.Parse(lds.PetDefense);
                MoveSpeed = Convert.ToSingle(lds.PetMoveSpeed);
                AttackSpeed = Convert.ToSingle(lds.PetAttackSpeed);
                CandyCost = Int32.Parse(lds.PetCandyCost);
                attackRange = Convert.ToSingle(lds.PetAttackRange);
                ExtraSkill = lds.PetExtraSkill;
           
        }
    }
}
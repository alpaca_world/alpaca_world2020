using System.Net.Mail;
using UnityEngine;
using QFramework;

// 1.请在菜单 编辑器扩展/Namespace Settings 里设置命名空间
// 2.命名空间更改后，生成代码之后，需要把逻辑代码文件（非 Designer）的命名空间手动更改
namespace QFramework.Example
{
	public partial class PetPickBtns : ViewController
	{
        
		void Start()
		{

			// Code Here
            this.PetBtn1.onClick.AddListener(() =>
            {
				//更换生成
                int candyPlayer = GameObject.Find("GameRoot").GetComponent<GameRoot>().playerCandyCount;
                if (candyPlayer >= 20)
                {
                    GameObject.Find("GameRoot").SendMessage("CreatePetObject", "0");
                    GameObject.Find("GameRoot").GetComponent<GameRoot>().decreCandyCount(20);
                }

            });
        }
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using OfficeOpenXml;
using QFramework;
using UnityEngine;
using Debug = UnityEngine.Debug;


/***
*  todo: 关卡数据读取系统
*/
public class LevelDataReader
{
    private string MySheetName = "Sheet1";
    //private readonly string fileName = Application.dataPath + "/test3.xlsx";
    public string fileName = FilePath.StreamingAssetsPath + "levelDataStruct.xlsx";
    // public string fileName = @"C:/levelDataStruct.xlsx";
    
    public List<string> MyString;
    private List<LevelData> levelDatas = new List<LevelData>();
    private List<PetData> petDatas = new List<PetData>();
    private List<HouseData> houseDatas = new List<HouseData>();

    public List<LevelData> GetLevelDatasFromExcel()
    {

        //获取excel文件
        FileInfo newFile = new FileInfo(fileName);
        LevelData tmp = new LevelData();
        PropertyInfo[] pis = tmp.GetType().GetProperties();
        int len = pis.Length;
        
            using (ExcelPackage package = new ExcelPackage(newFile))
            {
                //ExcelWorksheet sheet = package.Workbook.Worksheets[1]; //根据下标获取Sheet
                ExcelWorksheet worksheet_Data = package.Workbook.Worksheets[1]; //根据sheet名获取Sheet
                int maxRow_Data = worksheet_Data.Dimension.End.Row; //行
                int maxColumn_Data = worksheet_Data.Dimension.End.Column; //列
             
                //按照列索引字典，从数据Sheet中索取数据列
                //实际数据从第二行开始
                for (int i = 2; i <= maxRow_Data; i++)
                {
                    int index = 0;
                    LevelData lds = new LevelData();
                    for (int j = 1; j <= maxColumn_Data && index < len; j++, index++)
                    {
                        object val = worksheet_Data.Cells[i, j].Value;
                        //不使用Trim()。认为前后空格是有效数据，填表者应对数据的正确性负责。
                        if (val.IsNull()) val = "0";
                        string data = Convert.ToString(val.ToString().Trim());
                        // Debug.Log("data = " + maxRow_Data+":"+maxColumn_Data);
                        pis[index].SetValue(lds, Convert.ChangeType(data, pis[index].PropertyType), null); //类型转换。
                    }
                    levelDatas.Add(lds);

                }
            }
        


    return levelDatas;
    }
    public List<PetData> GetPetDatasFromExcel()
    {
        //获取excel文件
        FileInfo newFile = new FileInfo(fileName);
        PetData tmp = new PetData();
        PropertyInfo[] pis = tmp.GetType().GetProperties();
        int len = pis.Length;

        using (ExcelPackage package = new ExcelPackage(newFile))
        {
            ExcelWorksheet worksheet_Data = package.Workbook.Worksheets[2]; //根据sheet名获取Sheet
            int maxRow_Data = worksheet_Data.Dimension.End.Row; //行
            int maxColumn_Data = worksheet_Data.Dimension.End.Column; //列
  
            //按照列索引字典，从数据Sheet中索取数据列
            //实际数据从第二行开始
            for (int i = 2; i <= maxRow_Data; i++)
            {
                int index = 0;
                PetData lds = new PetData();
                for (int j = 1; j <= maxColumn_Data && index < len; j++, index++)
                {
                    object val = worksheet_Data.Cells[i, j].Value;
                    //不使用Trim()。认为前后空格是有效数据，填表者应对数据的正确性负责。
                    if (val.IsNull()) val = "0";
                    string data = Convert.ToString(val.ToString().Trim());
                    // Debug.Log("data = " + maxRow_Data+":"+maxColumn_Data);
                    pis[index].SetValue(lds, Convert.ChangeType(data, pis[index].PropertyType), null); //类型转换。
                }
                petDatas.Add(lds);

            }
        }



        return petDatas;
    }
    public List<HouseData> GetHouseDatasFromExcel()
    {
        //获取excel文件
        FileInfo newFile = new FileInfo(fileName);
        HouseData tmp = new HouseData();
        PropertyInfo[] pis = tmp.GetType().GetProperties();
        int len = pis.Length;

        using (ExcelPackage package = new ExcelPackage(newFile))
        {
            //ExcelWorksheet sheet = package.Workbook.Worksheets[1]; //根据下标获取Sheet
            ExcelWorksheet worksheet_Data = package.Workbook.Worksheets[3]; //根据sheet名获取Sheet
            int maxRow_Data = worksheet_Data.Dimension.End.Row; //行
            int maxColumn_Data = worksheet_Data.Dimension.End.Column; //列
    
            //按照列索引字典，从数据Sheet中索取数据列
            //实际数据从第二行开始
            for (int i = 2; i <= maxRow_Data; i++)
            {
                int index = 0;
                HouseData lds = new HouseData();
                for (int j = 1; j <= maxColumn_Data && index < len; j++, index++)
                {
                    object val = worksheet_Data.Cells[i, j].Value;
                    //不使用Trim()。认为前后空格是有效数据，填表者应对数据的正确性负责。
                    if (val.IsNull()) val = "0";
                    string data = Convert.ToString(val.ToString().Trim());
                    // Debug.Log("data = " + maxRow_Data+":"+maxColumn_Data);
                    pis[index].SetValue(lds, Convert.ChangeType(data, pis[index].PropertyType), null); //类型转换。
                }
                houseDatas.Add(lds);

            }
        }



        return houseDatas;
    }
}

public class HouseData
{
 

    public HouseData()
    {
    }

   

    private string houseName;
    private string houseLife;
    private string houseAttack;
    private string houseDefense;
    private string houseMoveSpeed;
    private string houseAttackSpeed;
    private string houseCandyCost;
    private string houseAttackRange;

    public HouseData(string houseName, string houseLife, string houseAttack, string houseDefense, string houseMoveSpeed, string houseAttackSpeed, string houseCandyCost, string houseAttackRange, string houseExtraSkill)
    {
        this.houseName = houseName;
        this.houseLife = houseLife;
        this.houseAttack = houseAttack;
        this.houseDefense = houseDefense;
        this.houseMoveSpeed = houseMoveSpeed;
        this.houseAttackSpeed = houseAttackSpeed;
        this.houseCandyCost = houseCandyCost;
        this.houseAttackRange = houseAttackRange;
        this.houseExtraSkill = houseExtraSkill;
    }

    public string HouseName
    {
        get => houseName;
        set => houseName = value;
    }

    public string HouseLife
    {
        get => houseLife;
        set => houseLife = value;
    }

    public string HouseAttack
    {
        get => houseAttack;
        set => houseAttack = value;
    }

    public string HouseDefense
    {
        get => houseDefense;
        set => houseDefense = value;
    }

    public string HouseMoveSpeed
    {
        get => houseMoveSpeed;
        set => houseMoveSpeed = value;
    }

    public string HouseAttackSpeed
    {
        get => houseAttackSpeed;
        set => houseAttackSpeed = value;
    }

    public string HouseCandyCost
    {
        get => houseCandyCost;
        set => houseCandyCost = value;
    }

    public string HouseAttackRange
    {
        get => houseAttackRange;
        set => houseAttackRange = value;
    }

    public string HouseExtraSkill
    {
        get => houseExtraSkill;
        set => houseExtraSkill = value;
    }

    public override string ToString()
    {
        return $"{nameof(houseName)}: {houseName}, {nameof(houseLife)}: {houseLife}, {nameof(houseAttack)}: {houseAttack}, {nameof(houseDefense)}: {houseDefense}, {nameof(houseMoveSpeed)}: {houseMoveSpeed}, {nameof(houseAttackSpeed)}: {houseAttackSpeed}, {nameof(houseCandyCost)}: {houseCandyCost}, {nameof(houseAttackRange)}: {houseAttackRange}, {nameof(houseExtraSkill)}: {houseExtraSkill}";
    }

    private string houseExtraSkill;
}
public class PetData
{
    public PetData(string petName, string petLife, string petAttack, string petDefense, string petMoveSpeed, string petAttackSpeed, string petCandyCost, string petAttackRange, string petExtraSkill)
    {
        this.petName = petName;
        this.petLife = petLife;
        this.petAttack = petAttack;
        this.petDefense = petDefense;
        this.petMoveSpeed = petMoveSpeed;
        this.petAttackSpeed = petAttackSpeed;
        this.petCandyCost = petCandyCost;
        this.petAttackRange = petAttackRange;
        this.petExtraSkill = petExtraSkill;
    }

    public PetData()
    {
    }

    private string petName;
    private string petLife;
    private string petAttack;
    private string petDefense;
    private string petMoveSpeed;
    private string petAttackSpeed;
    private string petCandyCost;
    private string petAttackRange;
    private string petExtraSkill;
    public override string ToString()
    {
        return $"{nameof(petName)}: {petName}, {nameof(petLife)}: {petLife}, {nameof(petAttack)}: {petAttack}, {nameof(petDefense)}: {petDefense}, {nameof(petMoveSpeed)}: {petMoveSpeed}, {nameof(petAttackSpeed)}: {petAttackSpeed}, {nameof(petCandyCost)}: {petCandyCost}, {nameof(petAttackRange)}: {petAttackRange}, {nameof(petExtraSkill)}: {petExtraSkill}";
    }

    public string PetName
    {
        get => petName;
        set => petName = value;
    }

    public string PetLife
    {
        get => petLife;
        set => petLife = value;
    }

    public string PetAttack
    {
        get => petAttack;
        set => petAttack = value;
    }

    public string PetDefense
    {
        get => petDefense;
        set => petDefense = value;
    }

    public string PetMoveSpeed
    {
        get => petMoveSpeed;
        set => petMoveSpeed = value;
    }

    public string PetAttackSpeed
    {
        get => petAttackSpeed;
        set => petAttackSpeed = value;
    }

    public string PetCandyCost
    {
        get => petCandyCost;
        set => petCandyCost = value;
    }

    public string PetAttackRange
    {
        get => petAttackRange;
        set => petAttackRange = value;
    }

    public string PetExtraSkill
    {
        get => petExtraSkill;
        set => petExtraSkill = value;
    }
}
public class LevelData
{
    private string levelNo;
    private string levelName;
    private string levelEnemyAttackDesc;
    private string levelSuccessCondition;
    private string levelTutorial;
    private string levelStory;
    private string levelAward;
    public LevelData(string levelNo, string levelName, string levelEnemyAttackDesc, string levelSuccessCondition, string levelTutorial, string levelStory, string levelAward)
    {
        this.levelNo = levelNo;
        this.levelName = levelName;
        this.levelEnemyAttackDesc = levelEnemyAttackDesc;
        this.levelSuccessCondition = levelSuccessCondition;
        this.levelTutorial = levelTutorial;
        this.levelStory = levelStory;
        this.levelAward = levelAward;
    }

    public string LevelNo
    {
        get => levelNo;
        set => levelNo = value;
    }

    public string LevelName
    {
        get => levelName;
        set => levelName = value;
    }

    public string LevelEnemyAttackDesc
    {
        get => levelEnemyAttackDesc;
        set => levelEnemyAttackDesc = value;
    }

    public string LevelSuccessCondition
    {
        get => levelSuccessCondition;
        set => levelSuccessCondition = value;
    }

    public string LevelTutorial
    {
        get => levelTutorial;
        set => levelTutorial = value;
    }

    public string LevelStory
    {
        get => levelStory;
        set => levelStory = value;
    }

    public string LevelAward
    {
        get => levelAward;
        set => levelAward = value;
    }



    public LevelData()
    {
    }

    public override string ToString()
    {
        return $"{nameof(levelNo)}: {levelNo}, {nameof(levelName)}: {levelName}, {nameof(levelEnemyAttackDesc)}: {levelEnemyAttackDesc}, {nameof(levelSuccessCondition)}: {levelSuccessCondition}, {nameof(levelTutorial)}: {levelTutorial}, {nameof(levelStory)}: {levelStory}, {nameof(levelAward)}: {levelAward}";
    }

    //临时类
  
    
   



  
}
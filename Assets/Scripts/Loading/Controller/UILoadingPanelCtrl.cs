using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QFramework;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

// 1.请在菜单 编辑器扩展/Namespace Settings 里设置命名空间
// 2.命名空间更改后，生成代码之后，需要把逻辑代码文件（非 Designer）的命名空间手动更改
namespace QFramework.Example
{
	public partial class UILoadingPanelCtrl : ViewController
	{
        private ResLoader mResLoader = null;
        private AssetBundle ab;

        void Start()
        {
            mResLoader = ResLoader.Allocate();
            //加载服务器测试
            StartCoroutine(LoadSceneFromServer());
            // StartCoroutine(LoadSceneFromLocal());
        }
   
        IEnumerator LoadSceneFromServer()
        {
            string menuServerPath = "http://120.79.86.116/unity/ttgame/alpaca2020/win/";
            UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(menuServerPath + QAssetBundle.Menu_unity.BundleName);
            yield return request.SendWebRequest();
            ab = (request.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene(QAssetBundle.Menu_unity.MENU);
        }
        IEnumerator LoadSceneFromLocal()
        {
             mResLoader.LoadSync("resources://"+QAssetBundle.Menu_unity.MENU);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene(QAssetBundle.Menu_unity.MENU);
        }
        private void OnDestroy()
        {
            mResLoader = null;
        }

    }
}

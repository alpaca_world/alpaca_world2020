using Assets.Scripts.Game.Controller.Model;
using Assets.Scripts.Game.Model;
using UnityEngine;
using QFramework;

// 1.请在菜单 编辑器扩展/Namespace Settings 里设置命名空间
// 2.命名空间更改后，生成代码之后，需要把逻辑代码文件（非 Designer）的命名空间手动更改
namespace QFramework.Example
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;


    public class UIGamePanelCtrlData : QFramework.UIPanelData
    {
    }

    public partial class UIGamePanelCtrl : QFramework.UIPanel
    {

        protected override void ProcessMsg(int eventId, QFramework.QMsg msg)
        {
            throw new System.NotImplementedException();
        }

        protected override void OnInit(QFramework.IUIData uiData)
        {


            uiData = uiData as UIGamePanelCtrlData ?? new UIGamePanelCtrlData();
            this.BackButton.onClick.AddListener(() =>
            {
                //回菜单
                SceneManager.LoadScene(QAssetBundle.Menu_unity.MENU, LoadSceneMode.Single);

            });
            // please add init code here
        }

        public void setTowerText(string text)
        {
            this.TowerText.text = text;

        }
        public void setCandyText(string text)
        {
            this.CandyCounts.text = text;
        }
        public void setTimeText(string text)
        {
            this.TimeText.text = text;
        }
        public void setLevelText(string text)
        {
            this.LevelText.text = text;
        
        }
        public void showSuccess()
        {
            this.GameSuccessPanel.Show();
            RetryButton.onClick.AddListener(() =>
            {
                GameObject.Find("GameRoot").SendMessage("GameRetry");
            });
        }
        public void showGameOver()
        {
            // this.GameSuccessPanel.Show();
            // RetryButton.onClick.AddListener(() =>
            // {
            //     GameObject.Find("GameRoot").SendMessage("GameRetry");
            // });
        }
        public void setLevelData(List<Level> levels)
        {
            Level level = levels[1];
            this.setLevelText("第"+level.LevelNo+"关 "+ level.LevelName);
        }

        public void setHouses(House player, House enemy)
        {
            this.setCandyText(30+"");
            this.PlayerLife.text = "主角生命:" + player.Life;
        }
        protected override void OnOpen(QFramework.IUIData uiData)
        {
        }

        protected override void OnShow()
        {
        }

        protected override void OnHide()
        {
        }

        protected override void OnClose()
        {
        }
    }
}

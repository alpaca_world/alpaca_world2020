﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using QFramework;
using UnityEngine;

public class PetNormal : MonoBehaviour
{
    public string name;
    public int life;
    public int attack;
    public float speed;
    public int defense;
    public float attackSpeed;
    public int candyCost;
    public string extraSkill;

    private Transform targetTrans;
    private GameObject tmpGameObjectEnemy;
    private float tempSpeed = 0f;

    private List<GameObject> arrGoEnemy;

    private bool isOver = false;
    // Start is called before the first frame update
    void Start()
    {
        arrGoEnemy = new List<GameObject>();
        tempSpeed = speed;
        if (this.gameObject.tag == "Enemy")
        {
            this.targetTrans = GameObject.Find("PlayerHouse").transform;
        }
        else
        {
            this.targetTrans = GameObject.Find("TowerNormal").transform;
        }
    }

    public bool getIsOver()
    {
        return isOver;
    }
    // Update is called once per frame
    void Update()
    {
        // new Vector3(-8, -1, 0)
        //向敌方移动
        float step = speed * Time.deltaTime;
        int dir = 1;
        if (gameObject.tag == "Enemy")
        {
            dir = -1;
        }
        Vector3 newVector3 = targetTrans.localPosition - new Vector3(0.05f*GetGoCountSameTag()*dir, 0, 0);
        gameObject.transform.localPosition =
            Vector3.MoveTowards(gameObject.transform.localPosition, newVector3, step);
    }
    //得到现在还有同类存活
    int GetGoCountSameTag()
    {
        int retCount = 0;
        if ((this.gameObject.tag == "Enemy"))
        {
            GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");

            retCount = gos.Length;

        }
        else
        {
            GameObject[] gos = GameObject.FindGameObjectsWithTag("Player");
            retCount = gos.Length;

        }

        return retCount;
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy" && this.gameObject.tag != "Enemy")
        {
            
            //敌方hp-1
           CanAttackOther(true);
        }

        if (collision.gameObject.tag == "Player" && this.gameObject.tag != "Player")
        {
            //停下，进行攻击...
            CanAttackOther(true);
        }
    }
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy" && this.gameObject.tag != "Enemy" && speed != 0) 
        {
            speed = 0;
            //敌方hp-1
            if (collision.gameObject.activeSelf && gameObject.activeSelf)
                 StartCoroutine(DoAttack(collision.gameObject));
        }

        if (collision.gameObject.tag == "Player" && this.gameObject.tag != "Player" && speed != 0)
        {
            speed = 0;
            if (collision.gameObject.activeSelf && gameObject.activeSelf)
              StartCoroutine(DoAttack(collision.gameObject));
        }
        
    }
    public void DamageLife(int attack)
    {
        life = life - (attack-defense);
        
        if (life < 0)
        {
           StopCoroutine("DoAttack");
            this.gameObject.SetActive(false);
           
            isOver = true;
        }
    }

    public void CanAttackOther(bool canAttackOther)
    {
        if (canAttackOther)
        {
            speed = tempSpeed;
        }
    }
    IEnumerator DoAttack(GameObject enemy)
    {
        while (enemy.gameObject.activeSelf)
        {
            yield return new WaitForSeconds(1.0f / attackSpeed);
            if (!this.gameObject.activeSelf) yield break;
            if(enemy.gameObject.activeSelf)
            enemy.GetComponent<PetGo>().SendMessage("DamageLife", attack);
           
        }
         

    }
}

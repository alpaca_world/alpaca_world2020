﻿using System;
using UnityEngine;

namespace Assets.Scripts.Game.Model
{
    public class House
    {
        private string name;
        private int life;
        private int attack;
        private int defense;
        private float moveSpeed;
        private float attackSpeed;
        private int candyCost;
        private float attackRange;

        public override string ToString()
        {
            return $"{nameof(name)}: {name}, {nameof(life)}: {life}, {nameof(attack)}: {attack}, {nameof(defense)}: {defense}, {nameof(moveSpeed)}: {moveSpeed}, {nameof(attackSpeed)}: {attackSpeed}, {nameof(candyCost)}: {candyCost}, {nameof(attackRange)}: {attackRange}, {nameof(extraSkill)}: {extraSkill}";
        }

        public House()
        {
        }

        public string Name
        {
            get => name;
            set => name = value;
        }

        public int Life
        {
            get => life;
            set => life = value;
        }

        public int Attack
        {
            get => attack;
            set => attack = value;
        }

        public int Defense
        {
            get => defense;
            set => defense = value;
        }

        public float MoveSpeed
        {
            get => moveSpeed;
            set => moveSpeed = value;
        }

        public float AttackSpeed
        {
            get => attackSpeed;
            set => attackSpeed = value;
        }

        public int CandyCost
        {
            get => candyCost;
            set => candyCost = value;
        }

        public float AttackRange
        {
            get => attackRange;
            set => attackRange = value;
        }

        public string ExtraSkill
        {
            get => extraSkill;
            set => extraSkill = value;
        }

        public House(string name, int life, int attack, int defense, float moveSpeed, float attackSpeed, int candyCost, float attackRange, string extraSkill)
        {
            this.name = name;
            this.life = life;
            this.attack = attack;
            this.defense = defense;
            this.moveSpeed = moveSpeed;
            this.attackSpeed = attackSpeed;
            this.candyCost = candyCost;
            this.attackRange = attackRange;
            this.extraSkill = extraSkill;
        }

        public House(HouseData hd)
        {
            this.name = hd.HouseName;
            this.life = Int32.Parse(hd.HouseLife);
            this.attack = Int32.Parse(hd.HouseAttack);
            this.defense = Int32.Parse(hd.HouseDefense);
            this.moveSpeed = Convert.ToSingle(hd.HouseMoveSpeed);
            this.attackSpeed = Convert.ToSingle(hd.HouseAttackSpeed);
            this.candyCost = Int32.Parse(hd.HouseCandyCost);
            this.attackRange = Convert.ToSingle(hd.HouseAttackRange);
            this.extraSkill = hd.HouseExtraSkill;
        }

        private string extraSkill;

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Game.Controller.Model;
using QFramework.Example;
using UnityEngine;

public class EnemyPet : MonoBehaviour
{
    public Pet pet = new Pet();

    public Pet Pet
    {
        get => pet;
        set { pet = value; }
    }


    public bool isOver = false;
    public float moveStep = 0f;
    private float tmpSpeed = 0f;

    private Transform targetTrans;

    //所有在攻击范围的敌人
    private List<GameObject> inAttackRangeGos;

    // Start is called before the first frame update
    void Start()
    {
        inAttackRangeGos = new List<GameObject>();
        this.targetTrans = GameObject.Find("PlayerHouse").transform;
        tmpSpeed = pet.MoveSpeed;
    }

    private void GoTarget()
    {
        //前往目标

        moveStep = tmpSpeed * Time.deltaTime;
        //同类间的偏移
        Vector3 newVector3 = targetTrans.localPosition -
                             new Vector3(-0.05f * GetGoCountSameTag(), 0.05f * GetGoCountSameTag(), 0);
        gameObject.transform.localPosition =
            Vector3.MoveTowards(gameObject.transform.localPosition, newVector3, moveStep);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOver)
        {
            GoTarget();
        }

    }

    //得到现在还有多少同类存活
    int GetGoCountSameTag()
    {
        int retCount = 0;
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");
        retCount = gos.Length;
        return retCount;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        

        if (collision.gameObject.tag == "PlayerHouse")
        {
            Debug.Log("meet playehouse");
            //停止
            tmpSpeed = 0;
            inAttackRangeGos.Add(collision.gameObject);
            //攻击第一个对象
            if (inAttackRangeGos.Count >= 1)
                StartCoroutine(DoAttack(inAttackRangeGos[0]));
        }

    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHouse")
        {
            bool containsGo = inAttackRangeGos.Contains(collision.gameObject);
            if (containsGo)
                inAttackRangeGos.Remove(collision.gameObject);
            if (inAttackRangeGos.Count == 0)
                tmpSpeed = pet.MoveSpeed;
        }

    }

    IEnumerator DoAttack(GameObject enemy)
    {
        while (enemy.gameObject.activeSelf)
        {
            yield return new WaitForSeconds(1.0f / pet.AttackSpeed);
            if (enemy.gameObject.activeSelf)
                enemy.GetComponent<PlayerHouse>().SendMessage("DamageLife", pet.Attack);

        }
    }

    public void DamageLife(int attack)
    {
        //计算的真实伤害
        int realAttack = 0;
        //判断伤害是否超过防御
        if (attack > pet.Defense)
            realAttack = attack - pet.Defense;
        else
        {
            //没有破防，则伤害为1
            realAttack = 1;
        }

        //受伤后的生命值
        int damagedLife = pet.Life - realAttack;

        if (damagedLife <= 0)
        {
            //当前对象死亡处理
            GoDie();
        }
        else
        {
            pet.Life = damagedLife;
        }
    }

    private void GoDie()
    {
        StopCoroutine("DoAttack");
        this.gameObject.SetActive(false);
        isOver = true;
    }
}

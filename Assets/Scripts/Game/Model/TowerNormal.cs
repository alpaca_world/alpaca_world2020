﻿using QFramework;
using QFramework.Example;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerNormal : MonoBehaviour
{
    public string name;
    public int hp;
    public int attack;
    public GameObject gamePanelCtrl;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void setHp(int hp)
    {
        this.hp = hp;
        GameObject.Find("UIGamePanel").GetComponent<UIGamePanelCtrl>().setTowerText("" + hp);

    }
    public void DamageHp(int attack)
    {
        if (hp-attack> 0)
        {
            hp -= attack;
            
            // (gamePanelCtrl).GetComponent<UIGamePanelCtrl>().setText(hp + "");
            if (gamePanelCtrl)
            {
                Debug.Log("NOT NULL");
             
               // UIGamePanelCtrl viewController = (UIGamePanelCtrl)(gamePanelCtrl).GetComponent<UIGamePanelCtrl>();
                // viewController.setText("sdf");
             }
          
        }
        else
        {
            hp = 0;
            GameObject.Find("GameRoot").SendMessage("setGameOver");
        }
        GameObject.Find("UIGamePanel").GetComponent<UIGamePanelCtrl>().setTowerText("" + hp);
    }
}

using UnityEngine;
using QFramework;
using UnityEngine.PlayerLoop;

// 1.请在菜单 编辑器扩展/Namespace Settings 里设置命名空间
// 2.命名空间更改后，生成代码之后，需要把逻辑代码文件（非 Designer）的命名空间手动更改
namespace QFramework.Example
{
	public partial class PetRat : ViewController
	{
		void Start()
		{
			// Code Here
            this.LifeSlider.maxValue = this.GetComponent<EnemyPet>().pet.Life;
        }

        void Update()
        {
            this.LifeSlider.value = this.GetComponent<EnemyPet>().pet.Life;
        }
	}
}

using Assets.Scripts.Game.Model;
using UnityEngine;
using QFramework;

// 1.请在菜单 编辑器扩展/Namespace Settings 里设置命名空间
// 2.命名空间更改后，生成代码之后，需要把逻辑代码文件（非 Designer）的命名空间手动更改
namespace QFramework.Example
{
	public partial class PlayerHouse : ViewController
	{
        public House player = new House();

        public House Player
        {
            get => player;
            set
            {
                player = value;
            }
        }

        void Start()
		{

        }

        public void DamageLife(int attack)
        {
            //计算的真实伤害
            int realAttack = 0;
            //判断伤害是否超过防御
            if (attack > player.Defense)
                realAttack = attack - player.Defense;
            else
            {
                //没有破防，则伤害为1
                realAttack = 1;
            }
            //受伤后的生命值
            int damagedLife = player.Life - realAttack;

            if (damagedLife <= 0)
            {
                //当前对象死亡处理
                Debug.Log("game over");
                player.Life = 0;
                LifeSlider.value = 0;
                StopCoroutine("DoAttack");
                this.gameObject.SetActive(false);
                //show gameOver
                GameObject.Find("GameRoot").SendMessage("setGameOver");
            }
            else
            {
                player.Life = damagedLife;
                this.LifeSlider.value = player.Life;
            }

        }
    }
}

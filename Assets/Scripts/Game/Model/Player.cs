﻿
using System;

namespace Assets.Scripts.Game.Controller.Model
{
    public class Player
    {

        public int No
        {
            get => no;
            set => no = value;
        }

        public string Name
        {
            get => name;
            set => name = value;
        }

        public int Life
        {
            get => life;
            set => life = value;
        }

        public int Attack
        {
            get => attack;
            set => attack = value;
        }

        public int Defense
        {
            get => defense;
            set => defense = value;
        }

        public float MoveSpeed
        {
            get => moveSpeed;
            set => moveSpeed = value;
        }

        public float AttackSpeed
        {
            get => attackSpeed;
            set => attackSpeed = value;
        }

        public int CandyCost
        {
            get => candyCost;
            set => candyCost = value;
        }

        public float AttackRange
        {
            get => attackRange;
            set => attackRange = value;
        }

        public string NormalSkill
        {
            get => normalSkill;
            set => normalSkill = value;
        }

        public int ExtraSkillColdTimeSec
        {
            get => extraSkillColdTimeSec;
            set => extraSkillColdTimeSec = value;
        }

        public string AttackType
        {
            get => attackType;
            set => attackType = value;
        }

        public string Info
        {
            get => info;
            set => info = value;
        }

        public string More
        {
            get => more;
            set => more = value;
        }

        public int DefaultCandy
        {
            get => defaultCandy;
            set => defaultCandy = value;
        }

        public int CandyAdd
        {
            get => candyAdd;
            set => candyAdd = value;
        }

        private int no = 1;
        private string name="主角";
        private int life=50;
        private int attack=10;
        private int defense=3;
        private float moveSpeed=0;
        private float attackSpeed=1f;
        private int candyCost=0;
        private float attackRange=10;
        private string normalSkill="pet";
        private int extraSkillColdTimeSec=20;
        private string attackType = "water";
        private string info = "信息";
        private string more="more";
        private int defaultCandy=20;
        private int candyAdd=1;
        string getIndexValue(string str, int index)
        {
            return str.Split(',')[index];
        }
        public Player()
        {

        }
        

        public override string ToString()
        {
            return $"{nameof(no)}: {no}, {nameof(name)}: {name}, {nameof(life)}: {life}, {nameof(attack)}: {attack}, {nameof(defense)}: {defense}, {nameof(moveSpeed)}: {moveSpeed}, {nameof(attackSpeed)}: {attackSpeed}, {nameof(candyCost)}: {candyCost}, {nameof(attackRange)}: {attackRange}, {nameof(normalSkill)}: {normalSkill}, {nameof(extraSkillColdTimeSec)}: {extraSkillColdTimeSec}, {nameof(attackType)}: {attackType}, {nameof(info)}: {info}, {nameof(more)}: {more}, {nameof(defaultCandy)}: {defaultCandy}, {nameof(candyAdd)}: {candyAdd}";
        }
    }
}
﻿/****************************************************************************
 * 2020.8 015
 ****************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using QFramework;

namespace QFramework.Example
{
	public partial class PetBtn1
	{
		[SerializeField] public UnityEngine.UI.Text PetBtn1Text;

		public void Clear()
		{
			PetBtn1Text = null;
		}

		public override string ComponentName
		{
			get { return "PetBtn1";}
		}
	}
}

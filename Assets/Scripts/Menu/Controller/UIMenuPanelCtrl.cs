using System.Collections;
using UnityEngine;
using QFramework;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static QFramework.ResKit;

// 1.请在菜单 编辑器扩展/Namespace Settings 里设置命名空间
// 2.命名空间更改后，生成代码之后，需要把逻辑代码文件（非 Designer）的命名空间手动更改
namespace QFramework.Example
{
	public partial class UIMenuPanelCtrl : ViewController
	{
        private ResLoader mResLoader = null;
        private AssetBundle ab;

        void Start()
		{
            mResLoader = ResLoader.Allocate();
            //120.79.86.116
           //  mResLoader.LoadSync("resources://"+QAssetBundle.Game_unity.GAME);
			// Code Here
            this.Button.onClick.AddListener(() =>
            {
                StartCoroutine(LoadSceneFromServer());
                // SceneManager.LoadScene(QAssetBundle.Game_unity.GAME);
            });

        }
        IEnumerator LoadSceneFromServer()
        {
            string serverPath = "http://120.79.86.116/unity/ttgame/alpaca2020/win/";
            UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(serverPath + QAssetBundle.Game_unity.BundleName);
            yield return request.SendWebRequest();
            ab = (request.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
            yield return new WaitForSeconds(2f);
            SceneManager.LoadScene(QAssetBundle.Game_unity.GAME);
        }
        private void OnDestroy()
        {
            mResLoader = null;
        }


    }
}

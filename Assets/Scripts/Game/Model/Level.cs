﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.Controller.Model
{
    public class Level
    {
        private int levelNo;
        private string levelName;
        private int levelEnemyCounts;
        private int levelEnemyRounds;
        private float levelRoundsColdTimeSec;
        private string LevelSuccessCondition;
        private string LevelTutorial;
        private string LevelStory;
        private string LevelAward;

        public Level()
        {
        }

        public int LevelNo
        {
            get => levelNo;
            set => levelNo = value;
        }

        public string LevelName
        {
            get => levelName;
            set => levelName = value;
        }

        public int LevelEnemyCounts
        {
            get => levelEnemyCounts;
            set => levelEnemyCounts = value;
        }

        public int LevelEnemyRounds
        {
            get => levelEnemyRounds;
            set => levelEnemyRounds = value;
        }

        public float LevelRoundsColdTimeSec
        {
            get => levelRoundsColdTimeSec;
            set => levelRoundsColdTimeSec = value;
        }

        public string LevelSuccessCondition1
        {
            get => LevelSuccessCondition;
            set => LevelSuccessCondition = value;
        }

        public string LevelTutorial1
        {
            get => LevelTutorial;
            set => LevelTutorial = value;
        }

        public string LevelStory1
        {
            get => LevelStory;
            set => LevelStory = value;
        }

        public string LevelAward1
        {
            get => LevelAward;
            set => LevelAward = value;
        }

        public Level(int levelNo, string levelName, int levelEnemyCounts, int levelEnemyRounds, float levelRoundsColdTimeSec, string levelSuccessCondition, string levelTutorial, string levelStory, string levelAward)
        {
            this.levelNo = levelNo;
            this.levelName = levelName;
            this.levelEnemyCounts = levelEnemyCounts;
            this.levelEnemyRounds = levelEnemyRounds;
            this.levelRoundsColdTimeSec = levelRoundsColdTimeSec;
            LevelSuccessCondition = levelSuccessCondition;
            LevelTutorial = levelTutorial;
            LevelStory = levelStory;
            LevelAward = levelAward;
        }
        public static List<int> str2Intlst(string str)
        {
            List<int> alist = new List<int>();

            System.Text.RegularExpressions.MatchCollection match = System.Text.RegularExpressions.Regex.Matches(str, @"(?<number>(\+|bai-)?(0|[1-9]\d*)(\.\d*[0-9])?)");
            foreach (System.Text.RegularExpressions.Match mc in match)
            {

                alist.Add(Int32.Parse(mc.Result("${number}")));
            }
            return alist;
        }
        public Level(LevelData lds)
        {
            this.levelNo = Int32.Parse(lds.LevelNo);
            this.levelName = lds.LevelName;
           ;
            List<int> alist = new List<int>();
            alist = str2Intlst(lds.LevelEnemyAttackDesc);
            if (alist.Count == 3)
            {
                //地洞出现3波进攻，每波2只。10s间隔
                this.levelEnemyCounts = alist[0]*alist[1];
                this.levelEnemyRounds = alist[0];
                this.levelRoundsColdTimeSec = alist[2];
            }
            else
            {
                this.levelEnemyCounts = 1;
                this.levelEnemyRounds = 1;
                this.levelRoundsColdTimeSec = 5;
            }
           
            LevelSuccessCondition = lds.LevelSuccessCondition;
            LevelTutorial = lds.LevelTutorial;
            LevelStory = lds.LevelStory;
            LevelAward = lds.LevelAward;
        }

        public override string ToString()
        {
            return $"{nameof(levelNo)}: {levelNo}, {nameof(levelName)}: {levelName}, {nameof(levelEnemyCounts)}: {levelEnemyCounts}, {nameof(levelEnemyRounds)}: {levelEnemyRounds}, {nameof(LevelRoundsColdTimeSec)}: {LevelRoundsColdTimeSec}, {nameof(LevelSuccessCondition)}: {LevelSuccessCondition}, {nameof(LevelTutorial)}: {LevelTutorial}, {nameof(LevelStory)}: {LevelStory}, {nameof(LevelAward)}: {LevelAward}";
        }
    }
}
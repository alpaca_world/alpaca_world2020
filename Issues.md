# 问题交流
## 框架问题

### 1 在使用ViewPanel（UIKit的CreateCode）时
子物体不能有类似（1）（2）
因为在生成代码的时候，在.Desiner.cs文件中
		public UnityEngine.UI.Button PetBtn1;
		public UnityEngine.UI.Button PetBtn2;
		public UnityEngine.UI.Button PetBtn3;
        如果有括号
        就变成了
 		public UnityEngine.UI.Button PetBtn（1）;
		public UnityEngine.UI.Button PetBtn（2）;
		public UnityEngine.UI.Button PetBtn（3）;       
        这样就会报错了，不是变量的声明
### 2 使用UIPanel 的新prefab的时候，一定要进行一次AB打包再运行，特别是删除某一个prefab的时候

### 3 在使用嵌套的bind后，需要先对prefab进行保存，才能生成代码
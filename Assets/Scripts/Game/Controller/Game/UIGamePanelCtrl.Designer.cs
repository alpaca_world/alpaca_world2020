﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QFramework.Example
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;
    
    
    // Generate Id:255f61fd-b094-4f94-84b9-ee12aef492c3
    public partial class UIGamePanelCtrl
    {
        
        public const string NAME = "UIGamePanelCtrl";
        
        [SerializeField()]
        public UnityEngine.UI.Text TowerText;
        
        [SerializeField()]
        public UnityEngine.UI.Button BackButton;
        
        [SerializeField()]
        public UnityEngine.UI.Button PetBtn1;
        
        [SerializeField()]
        public UnityEngine.UI.Text PetBtn1Text;
        
        [SerializeField()]
        public UnityEngine.UI.Button PetBtn2;
        
        [SerializeField()]
        public UnityEngine.UI.Button PetBtn3;
        
        [SerializeField()]
        public UnityEngine.UI.Image GameSuccessPanel;
        
        [SerializeField()]
        public UnityEngine.UI.Button RetryButton;
        
        [SerializeField()]
        public UnityEngine.UI.Text TimeText;
        
        [SerializeField()]
        public UnityEngine.UI.Text LevelText;
        
        [SerializeField()]
        public UnityEngine.UI.Text CandyCounts;
        
        [SerializeField()]
        public UnityEngine.UI.Text PlayerLife;
        
        private UIGamePanelCtrlData mPrivateData = null;
        
        public UIGamePanelCtrlData Data
        {
            get
            {
                return mData;
            }
        }
        
        UIGamePanelCtrlData mData
        {
            get
            {
                return mPrivateData ?? (mPrivateData = new UIGamePanelCtrlData());
            }
            set
            {
                mUIData = value;
                mPrivateData = value;
            }
        }
        
        protected override void ClearUIComponents()
        {
            TowerText = null;
            BackButton = null;
            PetBtn1 = null;
            PetBtn1Text = null;
            PetBtn2 = null;
            PetBtn3 = null;
            GameSuccessPanel = null;
            RetryButton = null;
            TimeText = null;
            LevelText = null;
            CandyCounts = null;
            PlayerLife = null;
            mData = null;
        }
    }
}

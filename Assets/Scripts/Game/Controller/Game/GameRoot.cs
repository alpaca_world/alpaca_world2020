using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Game.Controller.Model;
using Assets.Scripts.Game.Model;
using UnityEngine;

// 1.请在菜单 编辑器扩展/Namespace Settings 里设置命名空间
// 2.命名空间更改后，生成代码之后，需要把逻辑代码文件（非 Designer）的命名空间手动更改
namespace QFramework.Example
{
    public partial class GameRoot : ViewController
    {
        public int playerCandyCount = 50;
        private bool isGameSuccess = false;
        private bool isGameOver = false;
        public GameObject[] playerGameObject;
        public GameObject enemyGameObject;

        private float time;
        //计时
        private float totalTime;
        private int allTime;
        private List<Level> levels;
        private Level ld;
        private Pet rat;
        private Pet petSheep;
        private House playerHouse;
        private House tmpPlayer;
        private House enemyHouse;
        private int tmpLevelEnemyCounts;
        private bool isGameStart = false;
        private void Start()
        {
            LevelDataInit();
        }

   
        private void LevelDataInit()
        {
            //关卡数据初始化
            levels = new List<Level>();
            var a = new LevelDataReader();
            var lds  = new List<LevelData>();

            lds = a.GetLevelDatasFromExcel();
          
            
            // 读取关卡数据到并创建实体类
            // 从第2行读取也就是 0 1 是因为前2行是头信息和初始信息 eeplus第一行没有读取
            //这里只读取关卡数据，不保存敌人信息
            var levelCount = lds.Count;
            // //创建4个关卡数据,第一关为初始信息，i==1 的时候才是第一关
            for (var i = 0; i < levelCount; i++)
            {
                var level = new Level(lds[i]);
                levels.Add(level);
            }

            
            //设置第一关数据
            GameObject.Find("UIGamePanelCtrl").GetComponent<UIGamePanelCtrl>().setLevelData(levels);

            //创建所有动物数据 从第0行开始 
            List<Pet> pets = new List<Pet>();
            var petDatas = new List<PetData>();
            petDatas = a.GetPetDatasFromExcel();
            for (var i = 0; i < petDatas.Count; i++)
            {
                Pet pet = new Pet(petDatas[i]);
                pets.Add(pet);
            }

            var houses = new List<House>();
            var houseDatas = new List<HouseData>();
            houseDatas = a.GetHouseDatasFromExcel();
            for (var i = 0; i < houseDatas.Count; i++)
            {
                House house = new House(houseDatas[i]);
                houses.Add(house);
            }

            playerHouse = houses[0];
            Debug.Log(playerHouse.ToString());
            PlayerHouse.Player = playerHouse;
            enemyHouse = houses[1];
            tmpPlayer = playerHouse;

            playerCandyCount = 80;
            GameObject.Find("UIGamePanelCtrl").GetComponent<UIGamePanelCtrl>().setHouses(playerHouse,enemyHouse);

            ld = levels[1];
            rat = pets[1];
            petSheep = pets[0];
            tmpLevelEnemyCounts = ld.LevelEnemyCounts;
        }

        private void Update()
        {
            if (!isGameOver)
            {
                // 检查游戏结束条件
                if (tmpLevelEnemyCounts <= 0 && GetGoCountSameTag() == 0)
                {
                    //关卡结束
                    setSuccess();
                }
                //玩家挑战失败

                if (tmpPlayer.Life < 0)
                {
                    isGameOver = true;
                    setGameOver();
                }
                if (tmpLevelEnemyCounts > 0 && time > 2)
                {

                    StartCoroutine(CreateObject());
                    time = 0;
                    
                }

                Timer2();
            }

            time += Time.deltaTime;
        }

        private void RetryLevel()
        {
            playerCandyCount = 30;
            tmpLevelEnemyCounts = ld.LevelEnemyCounts;

        }
        public void GameRetry()
        {
            allTime = 0;
            isGameOver = false;
            RetryLevel();

            time = 0;
            //计时
            totalTime = 0;
            allTime = 0;
            //删除所有
            var gos = GameObject.FindGameObjectsWithTag("Player");
            foreach (var go in gos)
            {
                Destroy(go);
            }
            var es = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var e in es)
            {
                Destroy(e);
            }
        }
        int GetGoCountSameTag()
        {
            int retCount = 0;
                GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");

                retCount = gos.Length;

            return retCount;
        }
        private void Timer2()
        {
            //累加每帧消耗时间
            totalTime += Time.deltaTime;
            if (totalTime >= 1) //每过1秒执行一次
            {
                GameObject.Find("UIGamePanelCtrl").SendMessage("setTimeText", allTime + "s");
                allTime += 1;
                totalTime = 0;
                playerCandyCount += 1;
                PlayerHouse.CandyValue.text = playerCandyCount + "";
            }
        }

        public int getCandyCount()
        {
            return playerCandyCount;
        }

        public void decreCandyCount(int candy)
        {
            playerCandyCount -= candy;
            if (playerCandyCount < 0)
                playerCandyCount = 0;
        }

   

        public void setSuccess()
        {
            isGameSuccess = true;
            GameObject.Find("UIGamePanelCtrl").SendMessage("showSuccess");
        }
        public void setGameOver()
        {
            isGameOver = true;
            GameObject.Find("UIGamePanelCtrl").SendMessage("showGameOver");
        }
        private IEnumerator CreateObject()
        {

            if (ld.LevelEnemyCounts == 0) ld.LevelEnemyCounts = 1;
            if (ld.LevelEnemyRounds == 0) ld.LevelEnemyRounds = 1;
            for (var i = 0; i <ld.LevelEnemyCounts / ld.LevelEnemyRounds; i++)
            {
                yield return new WaitForSeconds(0.2f);
                tmpLevelEnemyCounts--;
                var goEnemy = Instantiate(enemyGameObject, transform, true);
                //设置属性
                goEnemy.GetComponent<EnemyPet>().pet = rat;

            }
            yield break;
        }


        private void CreatePetObject(string no)
        {
            var playerGO = Instantiate(playerGameObject[Int32.Parse(no)], transform, true);
            //设置属性
            playerGO.GetComponent<PetGo>().pet = petSheep;
          
        }
    }
}